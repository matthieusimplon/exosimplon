-- Active: 1709545666751@@127.0.0.1@3306@exosimplon



DROP TABLE IF EXISTS Promo;

DROP TABLE IF EXISTS Student;
DROP TABLE IF EXISTS room;

DROP TABLE IF EXISTS center;
DROP TABLE IF EXISTS Region;
  DROP TABLE IF EXISTS Booking;

CREATE TABLE center(
    id INT PRIMARY KEY AUTO_INCREMENT,
   name VARCHAR(12),
   id_region INT,
   Foreign Key (id_region) REFERENCES region(id)
);

CREATE TABLE Region(
    id INT PRIMARY KEY AUTO_INCREMENT,
   name VARCHAR(12),

);


CREATE TABLE room (
    id INT PRIMARY KEY AUTO_INCREMENT,
     name VARCHAR(20), 
     capacity INT,
     color VARCHAR(20),
     id_center INT,
     Foreign Key (id_center) REFERENCES center(id)
);
CREATE TABLE Booking(
    id INT,
    startDate DATETIME,
    endDate DATETIME,
    id_Booking INT,
    Foreign Key (id_Booking) REFERENCES Booking(id)
);



CREATE TABLE Promo(
    id INT,
    startDate DATE,
    endDate DATE,
    studentNumber INT
);

CREATE TABLE Student(
    id INT,
   name VARCHAR(12)
);
